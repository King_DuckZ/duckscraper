/*  Copyright (C) 2015 Michele Santullo
 *
 *  This file is part of DuckScraper.
 *
 *  DuckScraper is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DuckScraper is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DuckScraper.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef id21E0A6F345D24C5D83D3B1F74EC810F7
#define id21E0A6F345D24C5D83D3B1F74EC810F7

#include <string>
#include <vector>
#include <exception>
#include <utility>

namespace duck {
	typedef std::vector<std::vector<std::pair<std::string, std::string>>> XPathBatchResults;

	class ParseError : public std::exception {
	public:
		ParseError ( int parLine, int parColumn, std::string parMessage );
		virtual const char* what ( void ) const noexcept;
	private:
		std::vector<char> m_msg;
	};

	XPathBatchResults xpath_query ( const std::string& parXML, const std::vector<std::string>& parQueries );
} //namespace duck

#endif
