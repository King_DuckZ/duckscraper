/*  Copyright (C) 2015 Michele Santullo
 *
 *  This file is part of DuckScraper.
 *
 *  DuckScraper is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DuckScraper is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DuckScraper.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "xpath.hpp"
#include <pugixml.hpp>
#include <sstream>
#include <stdexcept>
#include <algorithm>

namespace duck {
	namespace {
		typedef std::pair<int, int> LineColType;

		LineColType line_col_from_offset (ptrdiff_t parOffset, const std::string& parData) {
			size_t index = 0;
			int line = 1;
			int chara = 1;
			while (parOffset and index < parData.size()) {
				if (parData[index] == '\n') {
					chara = 1;
					++line;
				}
				else {
					++chara;
				}
				++index;
				--parOffset;
			}
			return std::make_pair(line, chara);
		}
	} //unnamed namespace

	XPathBatchResults xpath_query (const std::string& parXML, const std::vector<std::string>& parQueries) {
		pugi::xml_document doc;
		std::istringstream iss(parXML);
		pugi::xml_parse_result result(doc.load(iss));
		if (not result) {
			auto line_col = line_col_from_offset(result.offset, parXML);
			throw ParseError(line_col.first, line_col.second, result.description());
		}

		XPathBatchResults retval;
		for (const auto& xpath : parQueries) {
			pugi::xpath_node_set xpathRes = doc.select_nodes(xpath.c_str());
			std::vector<std::pair<std::string, std::string>> new_lst;
			for (pugi::xpath_node_set::const_iterator itFind(xpathRes.begin()), itFindEND(xpathRes.end()); itFind != itFindEND; ++itFind) {
				const pugi::xpath_node& node = *itFind;
				std::pair<std::string, std::string> new_itm;
				if (node.node()) {
					new_itm.first = std::string(node.node().name());
					new_itm.second = std::string(node.node().value());
				}
				else if (node.attribute()) {
					new_itm.first = std::string(node.attribute().name());
					new_itm.second = std::string(node.attribute().value());
				}
				new_lst.push_back(std::move(new_itm));
			}
			retval.push_back(std::move(new_lst));
		}
		return std::move(retval);
	}

	ParseError::ParseError (int parLine, int parColumn, std::string parMessage) {
		std::ostringstream oss;
		oss << "Error parsing the source XML at line " <<
			parLine << " col " << parColumn << ":\n" <<
			parMessage << std::endl;
		auto msg = oss.str();
		m_msg.resize(msg.size() + 1);
		std::copy(msg.begin(), msg.end(), m_msg.begin());
		m_msg[m_msg.size() - 1] = '\0';
	}

	const char* ParseError::what() const noexcept {
		return m_msg.data();
	}
} //namespace duck
