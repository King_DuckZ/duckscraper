/*  Copyright (C) 2015 Michele Santullo
 *
 *  This file is part of DuckScraper.
 *
 *  DuckScraper is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DuckScraper is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DuckScraper.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef idC6776D903059465191FFB64FCFD6B86A
#define idC6776D903059465191FFB64FCFD6B86A

#include <string>

namespace duck {
	std::string fetch_html ( const std::string& parSource, std::string parUserAgent, bool parSslVerifyPeer, bool parSslVerifyHost );
	std::string clean_html ( std::string&& html );
} //namespace duck

#endif
