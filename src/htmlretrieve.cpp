/*  Copyright (C) 2015 Michele Santullo
 *
 *  This file is part of DuckScraper.
 *
 *  DuckScraper is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DuckScraper is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DuckScraper.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "htmlretrieve.hpp"
#include <ciso646>
#include <tidy.h>
#include <tidybuffio.h>
#include <sstream>
#include <curl_easy.h>
#include <cstring>
#include <stack>
#include <algorithm>
#include <memory>
#include <cassert>
#include <utility>

namespace duck {
	namespace {
		void dropScriptTags (std::string& html) {
			size_t open_index = 0;
			const std::string open_tag("<script");
			const std::string close_tag("</script>");

			while (html.npos != (open_index = html.find(open_tag, open_index))) {
				assert(open_index < html.size());
				auto close_index = html.find(close_tag, open_index + open_tag.size());
				if (close_index == html.npos)
					close_index = html.size();
				html.erase(open_index, std::min(html.size(), close_index + close_tag.size()) - open_index);
			}
		}

		bool isHttps (const std::string& parUrl) {
			const char protocol[] = "https://";
			const size_t protocolLen = sizeof(protocol) / sizeof(protocol[0]) - 1;
			if (parUrl.size() < protocolLen)
				return false;

			return std::equal(protocol, protocol + protocolLen, parUrl.begin());
		}
	} //unnamed namespace

	std::string clean_html (std::string&& html) {
		dropScriptTags(html);

		// Initialize a Tidy document
		TidyDoc tidyDoc = tidyCreate();
		TidyBuffer tidyOutputBuffer = {nullptr, nullptr, 0, 0, 0};

		// Configure Tidy
		// The flags tell Tidy to output XML and disable showing warnings
		bool configSuccess = tidyOptSetBool(tidyDoc, TidyXmlOut, yes)
			&& tidyOptSetBool(tidyDoc, TidyQuiet, yes)
			&& tidyOptSetBool(tidyDoc, TidyNumEntities, yes)
			&& tidyOptSetBool(tidyDoc, TidyShowWarnings, no)
			&& tidyOptSetBool(tidyDoc, TidyDropPropAttrs, yes);

		int tidyResponseCode = -1;

		// Parse input
		if (configSuccess) {
			tidyResponseCode = tidyParseString(tidyDoc, html.c_str());
		}

		// Process HTML
		if (tidyResponseCode >= 0)
			tidyResponseCode = tidyCleanAndRepair(tidyDoc);

		if (tidyResponseCode >= 0)
			tidyResponseCode = tidyRunDiagnostics(tidyDoc);
		if (tidyResponseCode > 1)
			tidyResponseCode = (tidyOptSetBool(tidyDoc, TidyForceOutput, yes) ? tidyResponseCode : -1);

		// Output the HTML to our buffer
		if (tidyResponseCode >= 0)
			tidyResponseCode = tidySaveBuffer(tidyDoc, &tidyOutputBuffer);

		// Any errors from Tidy?
		if (tidyResponseCode < 0)
			throw ("Tidy encountered an error while parsing an HTML response. Tidy response code: " + tidyResponseCode);

		// Grab the result from the buffer and then free Tidy's memory
		std::string tidyResult = (char*)tidyOutputBuffer.bp;
		tidyBufFree(&tidyOutputBuffer);
		tidyRelease(tidyDoc);

		return tidyResult;
	}


	std::string fetch_html (const std::string& parSource, std::string parUserAgent, bool parSslVerifyPeer, bool parSslVerifyHost) {
		using curl::curl_easy;
		using curl::curl_pair;

		std::ostringstream oss;
		curl_writer wr(oss);
		curl_easy easy(wr);
		easy.add(curl_pair<CURLoption, std::string>(CURLOPT_URL, parSource));
		if (isHttps(parSource)) {
			easy.add(curl_pair<CURLoption, bool>(CURLOPT_SSL_VERIFYPEER, parSslVerifyPeer));
			easy.add(curl_pair<CURLoption, bool>(CURLOPT_SSL_VERIFYHOST, parSslVerifyHost));
		}
		easy.add(curl_pair<CURLoption, std::string>(CURLOPT_USERAGENT, parUserAgent));
		easy.add(curl_pair<CURLoption, long>(CURLOPT_FOLLOWLOCATION, 1L));

		//try {
			easy.perform();
		//}
		//catch (curl_error& err) {
			//std::stack<std::pair<std::string, std::string> > errors = err.what();
			//err.print_traceback();
			//return 1;
		//}

		return oss.str();
	}
} //namespace duck
