/*  Copyright (C) 2015 Michele Santullo
 *
 *  This file is part of DuckScraper.
 *
 *  DuckScraper is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DuckScraper is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DuckScraper.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef id84F335333BEB461BB3FB74293DBDA4FC
#define id84F335333BEB461BB3FB74293DBDA4FC

#include <boost/program_options/variables_map.hpp>

namespace duck {
	bool parse_commandline ( int parArgc, char* parArgv[], boost::program_options::variables_map& parVarMap );
} //namespace duck

#endif
