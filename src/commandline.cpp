/*  Copyright (C) 2015 Michele Santullo
 *
 *  This file is part of DuckScraper.
 *
 *  DuckScraper is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DuckScraper is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DuckScraper.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "commandline.hpp"
#include "duckscraperConfig.h"
#include <boost/program_options.hpp>
#include <iostream>
#include <stdexcept>
#include <ciso646>

#define STRINGIZE_IMPL(s) #s
#define STRINGIZE(s) STRINGIZE_IMPL(s)

namespace po = boost::program_options;

namespace duck {
	namespace {
		const char* const g_version_string =
			PROGRAM_NAME " v"
			STRINGIZE(VERSION_MAJOR) "."
			STRINGIZE(VERSION_MINOR) "."
			STRINGIZE(VERSION_PATCH)
#if VERSION_BETA
			"b"
#endif
			;
	} //unnamed namespace

	bool parse_commandline (int parArgc, char* parArgv[], po::variables_map& parVarMap) {
		po::options_description desc("General");
		desc.add_options()
			("help,h", "Produces this help message")
			("version", "Prints the program's version and quits")
			("dump,d", po::value<std::string>(), "Cleans the retrieved html and saves it to the named file; use - for stdout")
			("dump-raw,D", po::value<std::string>(), "Saves the retrieved html to the named file; use - for stdout")
		;
		po::options_description query_options("Query options");
		query_options.add_options()
			("agent", po::value<std::string>()->default_value(DEFAULT_USER_AGENT), "User agent that will be passed to the server")
		;
		po::options_description positional_options("Positional options");
		positional_options.add_options()
			("input-url", po::value<std::string>(), "Input URL")
			("xpath", po::value<std::string>(), "XPath expression")
		;
		po::options_description all("Available options");
		all.add(desc).add(positional_options).add(query_options);
		po::positional_options_description pd;
		pd.add("input-url", 1).add("xpath", 1);
		try {
			po::store(po::command_line_parser(parArgc, parArgv).options(all).positional(pd).run(), parVarMap);
		}
		catch (const po::unknown_option& err) {
			throw std::invalid_argument(err.what());
		}

		po::notify(parVarMap);

		if (parVarMap.count("help")) {
#if !defined(NDEBUG)
			std::cout << "*******************\n";
			std::cout << "*** DEBUG BUILD ***\n";
			std::cout << "*******************\n";
			std::cout << '\n';
#endif
			po::options_description visible("Available options");
			visible.add(desc).add(query_options);
			std::cout << PROGRAM_NAME << " Copyright (C) 2015  Michele Santullo\n";
			std::cout << "This program comes with ABSOLUTELY NO WARRANTY.\n"; //for details type `show w'.
			std::cout << "This is free software, and you are welcome to\n";
			std::cout << "redistribute it under certain conditions.\n"; //type `show c' for details.
			std::cout << '\n';
			std::cout << "Usage: " << PROGRAM_NAME << " [options...] <url> <xpath>\n";
			std::cout << "You can pass - as the url to read from stdin\n";
			std::cout << visible;
			return true;
		}
		else if (parVarMap.count("version")) {
			std::cout << g_version_string;
			std::cout << " git revision " << VERSION_GIT << "\n";
			return true;
		}

		if (parVarMap.count("input-url") == 0) {
			throw std::invalid_argument("No input URL specified");
		}
		if (parVarMap.count("xpath") == 0) {
			throw std::invalid_argument("No XPath expression specified");
		}
		return false;
	}
} //namespace duck
