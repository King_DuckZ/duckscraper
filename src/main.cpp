/*  Copyright (C) 2015 Michele Santullo
 *
 *  This file is part of DuckScraper.
 *
 *  DuckScraper is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DuckScraper is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DuckScraper.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "htmlretrieve.hpp"
#include "commandline.hpp"
#include "xpath.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <utility>
#include <ciso646>
#include <memory>
#include <iterator>
#include <stdexcept>

namespace {
	void dump_string ( const std::string& parPathDest, const std::string& parData );
} //unnamed namespace

int main (int argc, char* argv[]) {
	using boost::program_options::variables_map;

	variables_map vm;
	try {
		if (duck::parse_commandline(argc, argv, vm)) {
			return 0;
		}
	}
	catch (const std::invalid_argument& err) {
		std::cerr << err.what() << "\nUse --help for help" << std::endl;
		return 2;
	}

	const auto url = vm["input-url"].as<std::string>();
	const auto xpath = vm["xpath"].as<std::string>();
#if !defined(NDEBUG)
	std::cout << "URL  : " << url << "\n";
	std::cout << "XPath: " << xpath << std::endl;
	std::cout << "Agent: " << vm["agent"].as<std::string>() << std::endl;
#endif

	std::string html;

	if ("-" != url) {
		html = duck::fetch_html(url, vm["agent"].as<std::string>(), false, false);
	}
	else {
		std::cin >> std::noskipws;
		std::istream_iterator<char> it(std::cin);
		std::istream_iterator<char> end;
		html = std::string(it, end);
	}

	if (vm.count("dump-raw")) {
		dump_string(vm["dump-raw"].as<std::string>(), html);
	}

	html = duck::clean_html(std::move(html));
	if (vm.count("dump")) {
		dump_string(vm["dump"].as<std::string>(), html);
	}

	try {
		std::vector<std::string> queries;
		queries.reserve(1);
		queries.push_back(std::move(xpath));
		auto results = duck::xpath_query(html, queries);
		for (const auto& lst : results[0]) {
			std::cout << lst.first << ": " << lst.second << '\n';
		}
	}
	catch (const duck::ParseError& err) {
		std::cerr << err.what() << std::endl;
		return 1;
	}

	return 0;
}

namespace {

	void dump_string (const std::string& parPathDest, const std::string& parData) {
		std::unique_ptr<std::ofstream> ofs;
		const bool use_stdout = ("-" == parPathDest);
		if (not use_stdout) {
			ofs.reset(new std::ofstream(parPathDest));
		}
		std::ostream* const os = (use_stdout ? &std::cout : ofs.get());
		*os << parData;
	}
} //unnamed namespace
