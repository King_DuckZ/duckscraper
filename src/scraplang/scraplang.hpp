/*  Copyright (C) 2015 Michele Santullo
 *
 *  This file is part of DuckScraper.
 *
 *  DuckScraper is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DuckScraper is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DuckScraper.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef idBE96C2D49C4C413888A79EAEB2B9C0FA
#define idBE96C2D49C4C413888A79EAEB2B9C0FA

#include <vector>
#include <string>
#include <memory>

namespace duck {
	struct ScrapNode;
	struct element_def;

	class ScrapNodePtr {
	public:
		explicit ScrapNodePtr ( ScrapNode* parPtr );
		ScrapNodePtr ( ScrapNodePtr&& parOther );
		~ScrapNodePtr ( void ) noexcept;

		ScrapNode& operator* ( void ) { return *m_ptr; }
		const ScrapNode& operator* ( void ) const { return *m_ptr; }
		ScrapNode& operator-> ( void ) { return *m_ptr; }
		const ScrapNode& operator-> ( void ) const { return *m_ptr; }

	private:
		std::unique_ptr<ScrapNode> m_ptr;
	};

	ScrapNodePtr parse_scraplang ( const std::string& parData );
	std::vector<element_def> get_xpath_definitions ( const ScrapNode& parAST );
} //namespace duck

#endif
