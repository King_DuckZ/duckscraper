/*  Copyright (C) 2015 Michele Santullo
 *
 *  This file is part of DuckScraper.
 *
 *  DuckScraper is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DuckScraper is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DuckScraper.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef id3875B5F868524EC3A1B83971D4A85777
#define id3875B5F868524EC3A1B83971D4A85777

#include <string>

namespace duck {
	enum ElementTypes {
		ElementType_String,
		ElementType_Integer,
		ElementType_Boolean,
		ElementType_Null,
		ElementType_Double
	};

	struct element_def {
		std::string name;
		std::string xpath;
		ElementTypes type;
	};
} //namespace duck

#endif
