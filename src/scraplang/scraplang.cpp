/*  Copyright (C) 2015 Michele Santullo
 *
 *  This file is part of DuckScraper.
 *
 *  DuckScraper is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DuckScraper is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DuckScraper.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "scraplang.hpp"
#include "scrapast.hpp"
#include "scraplang_visit_xpath.hpp"
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/fusion/adapted/struct.hpp>
#include <boost/fusion/adapted/std_pair.hpp>
#include <utility>

#include <boost/variant/apply_visitor.hpp>

namespace qi = boost::spirit::qi;
namespace sp = boost::spirit;

BOOST_FUSION_ADAPT_STRUCT(
	duck::element_def,
	(std::string, name)
	(std::string, xpath)
	(duck::ElementTypes, type)
)
BOOST_FUSION_ADAPT_STRUCT(
	duck::implem::node_list,
	(std::vector<duck::ScrapNode>, nodes)
)

namespace duck {
	namespace {
		struct ElementTypeSymbol : qi::symbols<char, ElementTypes> {
			ElementTypeSymbol() {
				add
					("string", ElementType_String)
					("integer", ElementType_Integer)
					("boolean", ElementType_Boolean)
					("null", ElementType_Null)
					("double", ElementType_Double)
				;
			}
		};

		template <typename I>
		struct ScrapGrammar : qi::grammar<I, ScrapNode(), sp::ascii::space_type> {
			ScrapGrammar() : ScrapGrammar::base_type(start) {
				using qi::lit;
				using qi::char_;
				using qi::lexeme;
				using qi::double_;
				using qi::int_;
				using qi::eps;

				start = whole;
				whole = eps >> *xpath_definition >> -map;
				xpath_definition = identifier >> lit('=') >> string >> "as" >> data_type;
				identifier = (char_('a', 'z') | char_('A', 'Z') | '_') >> *(char_('a', 'z') | char_('A', 'Z') | '_' | char_('0', '9'));
				string %= lexeme['"' >> +(char_ - '"') >> '"'];
				map = lit('{') >> ((identifier >> lit('=') >> value) % lit(',')) >> lit('}');
				array = lit('[') >> *(value % lit(',')) >> lit(']');
				value = string | double_ | int_ | array | map | identifier;
			}

			qi::rule<I, ScrapNode(), sp::ascii::space_type> start;
			qi::rule<I, implem::node_list(), sp::ascii::space_type> whole;
			qi::rule<I, element_def(), sp::ascii::space_type> xpath_definition;
			qi::rule<I, std::string(), sp::ascii::space_type> identifier;
			qi::rule<I, std::string(), sp::ascii::space_type> string;
			qi::rule<I, implem::map(), sp::ascii::space_type> map;
			qi::rule<I, implem::array(), sp::ascii::space_type> array;
			qi::rule<I, implem::element(), sp::ascii::space_type> value;
			ElementTypeSymbol data_type;
		};
	} //unnamed namespace

	ScrapNodePtr parse_scraplang (const std::string& parData) {
		ScrapGrammar<std::string::const_iterator> gramm;
		ScrapNodePtr retval(new ScrapNode);
		auto it_start = parData.cbegin();

		qi::phrase_parse(it_start, parData.cend(), gramm, sp::ascii::space, *retval);
		return std::move(retval);
	}

	std::vector<element_def> get_xpath_definitions (const ScrapNode& parAST) {
		std::vector<element_def> retval;
		implem::XPathVisitor xpath_vis(&retval);
		boost::apply_visitor(xpath_vis, parAST);
		return std::move(retval);
	}

	ScrapNodePtr::ScrapNodePtr (ScrapNode* parPtr) :
		m_ptr(parPtr)
	{
	}

	ScrapNodePtr::ScrapNodePtr (ScrapNodePtr&& parOther) :
		m_ptr(std::move(parOther.m_ptr))
	{
	}

	ScrapNodePtr::~ScrapNodePtr() noexcept {
	}
} //namespace duck
