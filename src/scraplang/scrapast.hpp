/*  Copyright (C) 2015 Michele Santullo
 *
 *  This file is part of DuckScraper.
 *
 *  DuckScraper is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DuckScraper is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DuckScraper.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef id9919CCB09DDD429C8128632F13D370ED
#define id9919CCB09DDD429C8128632F13D370ED

#include "scraplang_element.hpp"
#include <boost/spirit/include/support_extended_variant.hpp>
#include <string>
#include <vector>
#include <map>

namespace duck {
	struct ScrapNode;

	namespace implem {
		struct map;
		struct array;

		struct element : boost::spirit::extended_variant<
			boost::recursive_wrapper<map>,
			boost::recursive_wrapper<array>,
			std::string,
			int,
			double
		>
		{
			element ( void ) = default;
			element ( const map& parOther ) : base_type(parOther) {}
			element ( const array& parOther ) : base_type(parOther) {}
			element ( const std::string& parOther ) : base_type(parOther) {}
			element ( double parOther ) : base_type(parOther) {}
			element ( int parOther ) : base_type(parOther) {}
		};

		struct map : std::map<std::string, element> {
		};

		struct array : std::vector<element> {
		};

		struct node_list {
			std::vector<ScrapNode> nodes;
		};
	} //namespace implem

	struct ScrapNode : boost::spirit::extended_variant<
		element_def,
		implem::map,
		implem::node_list
	>
	{
		ScrapNode ( void ) = default;
		ScrapNode ( const element_def& parOther ) : base_type(parOther) {}
		ScrapNode ( const implem::map& parOther ) : base_type(parOther) {}
		ScrapNode ( const implem::node_list& parOther ) : base_type(parOther) {}
	};
} //namespace duck

#endif
