/*  Copyright (C) 2015 Michele Santullo
 *
 *  This file is part of DuckScraper.
 *
 *  DuckScraper is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DuckScraper is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DuckScraper.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef id7648347E8EE84E65B69018880358C8DF
#define id7648347E8EE84E65B69018880358C8DF

#include "scrapast.hpp"
#include <vector>

namespace duck {
	namespace implem {
		class XPathVisitor {
		public:
			typedef void result_type;

			explicit XPathVisitor ( std::vector<element_def>* parElements );

			void operator() ( const element_def& parElem );
			void operator() ( const implem::map& parMap );
			void operator() ( const node_list& parNodes );

		private:
			std::vector<element_def>* const m_elements;
		};

		inline XPathVisitor::XPathVisitor (std::vector<element_def>* parElements) :
			m_elements(parElements)
		{
		}

		inline void XPathVisitor::operator() (const element_def& parElem) {
			m_elements->push_back(parElem);
		}

		inline void XPathVisitor::operator() (const implem::map&) {
			return;
		}

		inline void XPathVisitor::operator() (const node_list& parNodes) {
			for (const auto& node : parNodes.nodes) {
				boost::apply_visitor(*this, node);
			}
		}
	} //namespace implem
} //namespace duck

#endif
